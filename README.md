# Arduino Library For The ADS8866 16-bit ADC

This library is an Arduino based driver for the ADS8866 16-bit ADC.

## Features

- Currently only 3-wire SPI mode is supported (no DIN).
- `read()` blocks until value is read back over SPI.

## Building And Running Tests

1. Make sure you have PlatformIO installed.

1. Make sure you can upload to a CanZero via NoCAN and that you have a UART connected from the CanZero RX/TX to your computer (only serial receive is needed, no transmit).

1. Clone this repository onto your local drive.

1. `cd` into the root directory of this repository.

1. To run the tests, run the command:

    ```bash
    pio test
    ```

    This will also build the code in `src/`.