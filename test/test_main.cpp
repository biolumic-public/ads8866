#include <functional>

#include <Arduino.h>
#include <unity.h>

#include <Ads8866.h>

using namespace ads8866;

class MockDigitalPin : public IDigitalPin {
public:
    void pinMode(PinMode pinMode) {
        m_pinMode = pinMode;
    }

    void write(PinStatus pinStatus) {
        m_pinStatus = pinStatus;
    }
private:
    PinMode m_pinMode = PinMode::INPUT;
    PinStatus m_pinStatus = PinStatus::LOW;
};

/** Mock HardwareSPI class for unit testing **/
class MockHardwareSPI : public HardwareSPI {
public:

    uint8_t transfer(uint8_t data) {
        return 0;
    };
    uint16_t transfer16(uint16_t data) {
        if (m_callbackTransfer16) return m_callbackTransfer16(data);
        return 0;
    };
    void transfer(void *buf, size_t count) {};
    void usingInterrupt(int interruptNumber) {};
    void notUsingInterrupt(int interruptNumber) {};
    void beginTransaction(SPISettings settings) {
        if (m_callbackBeginTransaction) return m_callbackBeginTransaction(settings);
    };
    void endTransaction(void) {
        if (m_callbackEndTransaction) return m_callbackEndTransaction();
    };
    void attachInterrupt() {};
    void detachInterrupt() {};
    void begin() {};
    void end() {};

    std::function<uint16_t(uint16_t data)> m_callbackTransfer16;
    std::function<void(SPISettings settings)> m_callbackBeginTransaction;
    std::function<void(void)> m_callbackEndTransaction;
};

MockDigitalPin* mockConvstPin;
MockHardwareSPI* mockHardwareSPI;
Ads8866* myAds8866;

void setUp(void) {
    mockConvstPin = new MockDigitalPin();
    mockHardwareSPI = new MockHardwareSPI();
    myAds8866 = new Ads8866(*mockHardwareSPI, *mockConvstPin, 1000);
}

void tearDown(void) {
    delete mockHardwareSPI;
    delete myAds8866;
}

void test_frequency_set_correctly(void) {
    TEST_ASSERT_EQUAL(1000, myAds8866->getClockFrequency_Hz());
}

void test_read(void) {

    SPISettings saved_settings;
    mockHardwareSPI->m_callbackBeginTransaction = [&](SPISettings settings) -> void {
        saved_settings = settings; 
    };

    mockHardwareSPI->m_callbackTransfer16 = [&](uint16_t data) -> uint16_t {
        return 0x1234;
    };

    bool endTransactionCalled = false;
    mockHardwareSPI->m_callbackEndTransaction = [&](void) -> void {
        endTransactionCalled = true;
    };

    auto adcValue = myAds8866->read();
    TEST_ASSERT_EQUAL(0x1234, adcValue);
    TEST_ASSERT_EQUAL(1000, saved_settings.getClockFreq());
    TEST_ASSERT_EQUAL(BitOrder::MSBFIRST, saved_settings.getBitOrder());
    TEST_ASSERT_EQUAL(SPIMode::SPI_MODE0, saved_settings.getDataMode());
    TEST_ASSERT_EQUAL(true, endTransactionCalled);
}

// Needed for floating point support in snprintf
asm(".global _printf_float");

// Weak empty variant initialization function.
// May be redefined by variant files.
void initVariant() __attribute__((weak));
void initVariant() { }

// Initialize C library
extern "C" void __libc_init_array(void);

/*
 * \brief Main entry point of Arduino application
 *
 * NOTE: We don't have the expression: if (serialEventRun) serialEventRun(); in this main() like
 * traditional Arduino programs as this does not merge well with the testing code style. However
 * we still seem to be able to send UART data just fine, so let's just leave it at that.
 */
int main( void )
{
    init();

    __libc_init_array();

    initVariant();

    delay(1);
#if defined(USBCON)
    USBDevice.init();
    USBDevice.attach();
#endif

    // Start serial for debugging purposes
    Serial.begin(115200);

    // Wait for serial to port to Start.
    // 2 seconds needed for Linux to enumerate.
    delay(2000);    

    UNITY_BEGIN();    // IMPORTANT LINE!

    RUN_TEST(test_frequency_set_correctly);
    RUN_TEST(test_read);

    UNITY_END();

    while(true) {
        // Block indefinitely
    }

    return 0;
}