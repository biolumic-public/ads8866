/**
 * Copyright (c) 2021 Biolumic Pte Ltd.  All rights reserved.
 * 
 * The copyright above and this notice must be preserved in all copies of this
 * source code.  The copyright above does not evidence any actual or intended
 * publication of this source code.
 * 
 * This is unpublished proprietary trade secret of Biolumic Pte Ltd.  This
 * source code may not be copied, disclosed, distributed, demonstrated or
 * licensed except as authorized by Biolumic Pte Ltd.
 * 
 * No part of this program or publication may be reproduced, transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual, or otherwise, without the prior written
 * permission of:
 * 
 * Biolumic Pte Ltd.
 * 21 Dairy Farm Road
 * Palmerston North 4410
 * New Zealand
 * 
 * @file Ads8866.h
 * 
 * @author Geoffrey Hunter
 * 
 */

#pragma once

#include <Arduino.h>
#include <SPI.h>

namespace ads8866 {

class IDigitalPin {
public:

    virtual void write(PinStatus pinStatus) = 0;
    virtual void pinMode(PinMode pinMode) = 0;
};

/**
 * Supports 3-wire mode.
 * */
class Ads8866 {
public:

    /**
     * Maximum SPI clock frequency as specified in the datasheet, in Hertz.
     */
    const uint32_t maxClockFrequency_Hz = 66600000;

    /**
     * Maximum time (in nanoseconds) required for an ADC conversion to complete, as per datasheet. 
     */
    const float tConvMax_ns = 8800.0;

    /**
     * Create a driver for the ADS8866 ADC. Configures the convstPin as a digital output and drives it low.
     * 
     * @param spi               The driver for the SPI bus the sensor is connected to.
     * @param convstPin         Arduino-style identifier for the Arduino pin connected to the ADC's 'CONVST' pin. (which functions as a chip select
     *                              in three wire mode).
     * @param clockFrequency    Desired clock frequency for the SPI communications when reading back data. Note this is NOT the ADC conversion clock
     *                              frequency, that is set by an independent clock which is internal to the IC.
     * @param assertCallback    Function callback which will be called when the library wants to raise an assert.
     * 
     * */
    Ads8866(HardwareSPI &spi, IDigitalPin &convstPin, uint32_t clockFrequency_Hz);

    /**
     * Perform a read from the ADC. This is a blocking call and will start a conversion, block for tConvMax_ns until
     * conversion is complete, and then read back the value. 
     * 
     * @returns The measured 16-bit value from the ADC. No scaling is performed, this is the raw data returned
     *              from the ADC and it's interpretation will depend on VREF and what is connected to pins ANN and ANP.
     * */
    uint16_t read();

    /**
     * Get the clock frequency the SPI will operate at, as set in the constructor.
     * 
     * @returns The set clock frequency, in Hz.
     */
    uint32_t getClockFrequency_Hz();

private:
    HardwareSPI &m_spi;
    IDigitalPin &m_convstPin;
    uint32_t m_clockFrequency_Hz;    
};

} // namespace ads8866