/**
 * Copyright (c) 2021 Biolumic Pte Ltd.  All rights reserved.
 * 
 * The copyright above and this notice must be preserved in all copies of this
 * source code.  The copyright above does not evidence any actual or intended
 * publication of this source code.
 * 
 * This is unpublished proprietary trade secret of Biolumic Pte Ltd.  This
 * source code may not be copied, disclosed, distributed, demonstrated or
 * licensed except as authorized by Biolumic Pte Ltd.
 * 
 * No part of this program or publication may be reproduced, transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual, or otherwise, without the prior written
 * permission of:
 * 
 * Biolumic Pte Ltd.
 * 21 Dairy Farm Road
 * Palmerston North 4410
 * New Zealand
 * 
 * @file Ads8866.cpp
 * 
 * @author Geoffrey Hunter
 * 
 */

#include <assert.h>

#include "Ads8866.h"

namespace ads8866 {

/**
 * Enable time: CONVST low to MSB valid (as per datasheet)
 */
const float tdCnvDo_ns = 12.3;

/**
 * Minimum time (in nanoseconds) required for an ADC conversion to complete, as per datasheet. 
 */
const float tConvMin_ns = 500;

Ads8866::Ads8866(HardwareSPI &spi, IDigitalPin &convstPin, uint32_t clockFrequency_Hz) :
    m_spi(spi),
    m_convstPin(convstPin),
    m_clockFrequency_Hz(clockFrequency_Hz)    
{
    assert(m_clockFrequency_Hz <= maxClockFrequency_Hz);

    // Make sure CONVST pin is configured low before driving pin to avoid
    // starting spurious conversions. This will also disable the pull-up
    m_convstPin.write(LOW); 
    m_convstPin.pinMode(OUTPUT);    
}

uint16_t Ads8866::read()
{
    // Drive CONVST high to start conversion. Din will be high when we do this
    // since it's pulled high by resistor (pin not connected to micro in 3-wire
    // mode)
    m_convstPin.write(HIGH);

    // We could now drive CONVST low to select other SPI devices, BUT...it must
    // be again driven high before the min. conversion time expires (tconv-min)

    // Wait tconv-max, then drive CONVST low
    delayMicroseconds(std::ceil(tConvMax_ns/1000.0));

    // First pin will now be available on DOUT pin of SPI bus. Subsequent bits
    // are clocked out on the falling edge of the clock signal.
    // Suitable SPI mode os CPOL=0, CPHA=0
    m_spi.beginTransaction(SPISettings(m_clockFrequency_Hz, MSBFIRST, SPI_MODE0));

    m_convstPin.write(LOW);

    // Wait td-CNV-DO=12.3ns for MSB to be valid after CONVST goes low
    // Such a small amount of time, we should be ok?

    // Clock in the 16-bits of ADC data. Send dummy 0x0000 data out
    // TODO: Verify that this correctly reads in the first bit, which
    // is already present before first clock negative edge, 
    // and only clocks in 15 more bits, not 16 
    // DOUT gets tri-stated on falling edge of 16th clock pulse
    uint16_t receivedData = m_spi.transfer16(0x0000);

    // Do not set CONV pin high again. This will initiate a new conversion, which
    // we do not want to start until requested by the user (by another call to read())

    // Finish transaction
    m_spi.endTransaction();

    // Conversion time varies, but is bounded by tconv given in datasheet
    return receivedData;
}

uint32_t Ads8866::getClockFrequency_Hz() {
    return m_clockFrequency_Hz;
}

} // namespace ads8866