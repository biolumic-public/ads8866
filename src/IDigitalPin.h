/**
 * Copyright (c) 2021 Biolumic Pte Ltd.  All rights reserved.
 * 
 * The copyright above and this notice must be preserved in all copies of this
 * source code.  The copyright above does not evidence any actual or intended
 * publication of this source code.
 * 
 * This is unpublished proprietary trade secret of Biolumic Pte Ltd.  This
 * source code may not be copied, disclosed, distributed, demonstrated or
 * licensed except as authorized by Biolumic Pte Ltd.
 * 
 * No part of this program or publication may be reproduced, transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual, or otherwise, without the prior written
 * permission of:
 * 
 * Biolumic Pte Ltd.
 * 21 Dairy Farm Road
 * Palmerston North 4410
 * New Zealand
 * 
 * @file Ads8866.h
 * 
 * @author Geoffrey Hunter
 * 
 */

#ifndef IDIGITALPIN_H
#define IDIGITALPIN_H

#include <Arduino.h>
#include <SPI.h>

class IDigitalPin {
public:

    virtual void write(PinStatus pinStatus) = 0;
    virtual void pinMode(PinMode pinMode) = 0;
};

#endif